// ==== We need to define the models now, because all database issued-commands will be issued through models via routes
var User            = require('../models/user.js');
var SubmissionTag   = require('../models/submission_tag.js');
var jwt         = require('jsonwebtoken'); // used to create, sign, and verify tokens
var config      = require('../config/config.js');
module.exports = function(app, express, passport){

    //======== FRONT PAGE =======================
    // ==========================================
    // ==========================================

    app.get('/', function(req, res){
       res.render('index');
    });


    // =============== SIGNUP ===================
    // ==========================================
    // ==========================================

    app.get('/signup', function(req, res){
        //res.sendFile('public/signup.ejs', {root: rootDir });
        res.render('signup.ejs', { message: req.flash('signupMessage') });
    });

    app.post('/signup', function(req, res){
        var username = req.body.username;
        var password = req.body.password;

        User.findOne({
            where: {
                user_name: username
            }
        }).then(function (user) {
            if (user) {
                console.log("The username: " + username + " is already taken.");

                // Failure --- > /signup
                res.json({
                    success: false,
                    message: 'The username is already taken'
                });
            } else {
                // If username isn't already taken, then create account

                User.methods.createAccount(username, password, req.body.email, function(newUser){
                    console.log("Success creating user");

                    // Give token
                    var token = jwt.sign({
                        id: newUser.id,
                        username: newUser.user_name
                    }, config.secret, {
                        expiresIn: 86400 // expires in 24 hours
                    });

                    console.log(token); //

                    res.json({
                        success: true,
                        message: 'New account created, enjoy your token!',
                        token: token
                    });
                });
            }
        }).catch(function(err){
            console.log(err);
        });

    });

    // =============== USER ====================
    // ==========================================
    // ==========================================

    app.get('/user/:user_name', function(req, res){
        console.log(req.session);
        res.send("Here is the user account for: " + req.params.user_name);
    });


    // ======= API Router ========================
    // ===========================================
    // ===========================================

    var apiRouter = express.Router();

    apiRouter.get('/', function(req, res){
        res.send('Hello! The API is at http://localhost:' + 3000 + '/api');
    });

    // ======= AUTHENTICATE ======================
    // ===========================================
    // ===========================================

    apiRouter.post('/auth', function(req, res){
       console.log(req.body);
       User.find({
           where: {
               user_name: req.body.username
           }
       }).then(function(user){
            console.log(user + " is the user");
           //User not found
           if(!user){
               res.json({
                   success: false,
                   message: 'Authentication failed. User not found.'
               })
           } else {
              //User found check if password matches
               user.comparePassword(req.body.password, function(err, result){

                   //If password matches
                   if(result){
                       // Password is correct, create a token
                       console.log(user);
                       var token = jwt.sign({
                           id: user.id,
                           username: user.user_name
                           //TODO: Replace this with the secret from config
                       }, config.secret, {
                           expiresIn: 86400 // expires in 24 hours
                       });

                       // return the information including token as JSON
                       res.json({
                           success: true,
                           message: 'Enjoy your token!',
                           token: token
                       });
                   } else {
                       //Passwords don't match
                       res.json({
                           success: false,
                           message: 'Authentication failed. Wrong password.'
                       });
                   }
               });
           }
       })
    });

    // ======= AUTHENTICATE ON EVERY REQUEST =====
    // ===========================================
    // ===========================================

    apiRouter.use(function(req, res, next){
        var token = req.body.token || req.query.token || req.headers['x-access-token'];

        if(token){

            jwt.verify(token, config.secret, function(err, decoded){

               if(err){
                   return res.json({success: false, message: 'Failed to authenticate token.'});
               } else {
                   //if the token checks out, then save to the reqest for use in other routes
                   req.decoded = decoded;
                   next();
               }
            });
        } else {
            //if there is no token
            res.status(403).send({
                success: false,
                message: 'No token provided.'
            })
        }
    });

    // route to return all users (GET http://localhost:3000/api/users)
    apiRouter.get('/users', function(req, res) {
        User.findAll({
            attributes: ['id','user_name', 'user_email']
        }).then(function(users){
           res.json(users);
        });
    });

    // route to return user by username (GET http://localhost:3000/api/user must be authenticated as this user this user)
    apiRouter.get('/user/:username', function(req, res){
        var username = req.param.username;
        console.log("GIMME " + username);

        var authenticatedUser = req.decoded;
        // Make sure that user is authenticated as user they are trying to get

        if(!authenticatedUser){
            res.json({
                success: false,
                message: "You could not prove auth to get this user"
            })
        } else {
            User.find({
                where: {
                    user_name: authenticatedUser.username
                }
            }).then(function(user){
                res.json({
                    success: true,
                    message: "User successfully retrieved",
                    user: {
                        id: user.id,
                        username: user.user_name,
                        email: user.user_email,
                        joindate: user.user_joindate,
                        karma: user.user_karma
                    }
                })
            })
        }
    });

    // localhost:3000/api/posts?encodedTags=WyJXZWIiXQ==&offset=0&limit=25
    /* The encoded tags is an array, btoa [""]
     */
    apiRouter.get('/posts', function(req, res){

        //Get headers

        var tags = new Buffer(req.query.encodedTags, 'base64').toString('binary');
        var offset = req.query.offset;
        var limit = req.query.limit;

        console.log(tags + " ***************** tags");
        tags = JSON.parse(tags);

        if(tags.length != 0){
            //Get all submissions by tag
            SubmissionTag.methods.getAllSubmissionsByTag(tags, limit, offset, function(submissions){
                console.log("AFTER PROMISE");
                console.log(submissions);

                var retObj = {
                    posts: {}
                };

                // Create Message
                submissions.forEach(function(submission) {

                    retObj.posts[submission.id] = {
                        id: submission.id,
                        title: submission.sub_title,
                        user: submission.user,
                        link: submission.sub_link,

                        karma: submission.sub_karma,
                        createdAt: submission.createdAt,
                        updatedAt: submission.updatedAt,
                        tags:submission.tags   // an array
                    };

                    retObj.success = true;
                    retObj.message = "Passed back all submissions considering tags";

                });

                //Send All Posts
                res.json(retObj);
            });
        } else {
            //Get all submissions
            SubmissionTag.methods.getAllSubmission(function(submissions){
                console.log(submissions);
            });

            res.json({
                success: true,
                message: "Passed back all submissions (like, really- all of them)"

            });
        }

    });


    // Pass the custom API router into the main router
    app.use('/api', apiRouter);

    // ====================================
    // FUNCTIONS ==========================
    // ====================================

    //routing middleware to make sure that a user is logged in
    function isLoggedIn(req, res, next){

        //if a user is authenticated, carry on
        if(req.isAuthenticated()){  //route middleware
            return next();
        }

        //otherwise, redirect them to the home page
        res.redirect('/login');
    }

    app.get('*', function(req, res, next) {
      res.render('index');
    });

};
