
// ========= Require Things We Need ============

var express     = require('express');               // simple HTTP webserver addon
var app         = express();                        // initialize express module
var path        = require('path');                  // used to pass files by location on disk via route endpoints
var bodyParser  = require('body-parser');           // used to get POST content from HTTP requests/forms
var morgan      = require('morgan');                // logs every request/res to the console
var port        = process.env.PORT || 3000;
var passport    = require('passport');



var cookieParser    = require('cookie-parser');
var config          = require('./config/config.js');
var session         = require('express-session');

var flash       = require('connect-flash');
var root        = __dirname;

// ============= CONFIGURE AUTH =============
require('./config/passport.js')(passport);

// ========= APP CONFIGURATION (setup middleware) ===================
// Body Parser
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.set('superSecret', config.secret);

app.use(function(req, res, next){
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type, Authorization');
    next();
});
app.use(morgan('dev')); // log every request to the console
app.use(cookieParser()); // read cookies (needed for auth)

//TODO: Replace this with whatever engine the other guys are using
app.set('view engine', 'ejs'); //setup using ejs for templates FOR NOW

    // Passport and Authentication
    //app.use(session({
    //    secret: 'thebirthdaypartyismyfavouriteband',
    //    cookie: { maxAge: 2628000000 }
    //}));
    //app.use(passport.initialize());
    //app.use(passport.session()); // persistent login sessions
    app.use(flash()); // use connect-flash for flash messages stored in session

app.use(express.static(__dirname + '/public'));
app.use(express.static(__dirname + '/views'));

// ============= INIT MODELS ================
require('./models');


// ================ ROUTES ==================
// ==========================================
require('./routes/routes.js')(app, express, passport, root);


app.listen(port);