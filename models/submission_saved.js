/*  |===================================
    |SAVED SUBMISSION MODEL=============
    |===================================
    |ss_submission_id                      
    |ss_saved_id                    
    |ss_saved_date                        
    |=================================== */

/* Required packages and files */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

var SubmissionSaved = sequelize.define('submission_saved', {
    ss_id: {
        type: Sequelize.INTEGER(8),
        field: 'ss_id',
        primaryKey: true,
        allowNull: false
    },
    ss_submission_id: {
        type: Sequelize.INTEGER(8),
        field: 'ss_submission_id',
        allowNull: false
    },
    ss_user_id: {
        type: Sequelize.INTEGER(8),
        field: 'ss_user_id',
        allowNull: false
    },
    ss_saved_date: {
        type: Sequelize.DATE(),
        field: 'ss_saved_date',
        allowNull: false
    }
});

/* Methods for saved submission model */
SubmissionSaved.methods = {
    createTableIfNotExists: function() {
        SubmissionSaved.sync();
    },
    createSavedSubmission: function() {
        console.log("Saving a submission...");
        
        console.log("DONE");
    },
    removeSavedSubmission: function() {
        console.log("Removing a saved submission...");
        
        console.log("DONE");
    },
    
    // #########################################################
    // TODO - Add needed methods.
    // #########################################################
    
    getId: function() {
        return this.ss_id;
    },
    getSubmissionId: function() {
        return this.ss_submission_id;
    },
    getUserId: function() {
        return this.ss_user_id;
    },
    getSavedDate: function() {
        return this.ss_saved_date;
    }
}

module.exports = SubmissionSaved;