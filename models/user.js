
// ============= ACCOUNT MODEL ============
// ========================================

    // ==== Get Sequelize package ====
    var Sequelize = require('sequelize');
    var bcrypt = require('bcrypt-nodejs'); // used for hashing the password to be saved

    // ==== Get Connection to MySQL Database via Sequelize configuration ====
    var sequelize = require('../config/config.js').dbConnection;

    // ==== Define Account Model ====

    var User = sequelize.define('user', {
        id: {
            type: Sequelize.INTEGER(8),
            primaryKey: true,
            autoIncrement: true
        },
       user_name: {
           type: Sequelize.STRING(25),
           field: 'user_name',
           allowNull: false
       },
       user_password: {
           type: Sequelize.STRING(300),
           field: 'user_password',
           allowNull: false
       },
       user_email: {
           type: Sequelize.STRING(50),
           field: 'user_email',
           allowNull: true
       },
        user_karma: {
            type: Sequelize.INTEGER(5),
            field: 'user_karma',
            defaultValue: 0
        },
        user_joindate: {
            type: Sequelize.DATEONLY(),
            field: 'user_joindate',
            allowNull: false
        }
    }, {
        instanceMethods: {
            updatePassword: function(password, done) {
                var user = this;
                bcrypt.hash(password, null, null, function(err, hash){
                    //if the hash to save the new password failed, return the error
                    if(err){
                        done(err, hash);
                    }

                    //otherwise, change the password and save it
                    user.updateAttributes({
                            user_password: hash
                        }).then (function(hello) {
                            done(err, user.user_password);
                        }).catch (function(err){
                            done(err);
                    });
                });
            },
            comparePassword: function(password, done) {
                var user = this;

                bcrypt.compare(password, user.user_password, function(err, res) {
                    done(err, res);
                });
            },
            getUserName: function()  {
                return this.user_name
            }
        }
    });


    // ==== Define Methods For Account Model
    User.methods = {
        createTableIfNotExists: function(){
            User.sync();
        },
        createAccount: function(username, pass, email, callback) {
            console.log("creating user: " + username);

            bcrypt.hash(pass, null, null, function(err, hash) {
                // Store user in database
                return User.create({
                    user_name: username,
                    user_password: hash,
                    user_email: email,
                    user_joindate: new Date()
                }).then(function (user) {
                    console.log("Success creating: " + user);
                    callback(user);
                }).catch(function (err) {
                    console.log("An error occured when creating an Account---->   " + err);
                });
            });
        }
    };


    // ==== Return Account Model ====

    module.exports = User;

