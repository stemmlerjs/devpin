/*  |===================================|
    |SUBSCRIPTION USER MODEL============|
    |===================================|
    |su_id                              |
    |su_subscriber_id                   |
    |su_provider_id                     |
    |==================================== */

/* Required packages */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

/* Define Submission model */
var Subscription = sequelize.define('subscription', {
    sub_id: {
        type: Sequelize.INTEGER(8),
        field: 'sub_id',
        primaryKey: true,
        allowNull: false
    },
    sub_subscriber_id: {
        type: Sequelize.INTEGER(8),
        field: 'sub_subscriber_id',
        allowNull: false
    },
    sub_provider_id: {
        type: Sequelize.INTEGER(8),
        field: 'sub_provider_id',
        allowNull: false
    }
});

/* Methods for submission model */
Subscription.methods = {
    createTableIfNotExists: function() {
        Subscription.sync();
    },
    addSubscription: function() {
        console.log("Creating new subscription for user...");
        
        console.log("DONE");
    },
    removeSubscription: function() {
        console.log("Removing subscription for user...");
        
        console.log("DONE");
    },
        
    // #########################################################
    // TODO - Add needed methods.
    // #########################################################
    
    getId: function() {
        return this.sub_id;
    },
    getSubscriberId: function() {
        return this.sub_subscriber_id;
    },
    getProviderId: function() {
        return this.sub_provider_id;
    }
}

module.exports = Subscription;