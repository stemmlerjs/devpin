/*  |===================================
    |VOTE SUBMISSION MODEL==============
    |===================================
    |sv_voter_id                      
    |sv_submission_id                 
    |sv_title                         
    |sv_count                         
    |==================================== */

/* Required packages */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

/* Define Submission model */
var SubmissionVote = sequelize.define('submission_vote', {
    sv_voter_id: {
        type: Sequelize.INTEGER(8),
        field: 'sv_voter_id',
        allowNull: false
    },
    sv_submission_id: {
        type: Sequelize.INTEGER(8),
        field: 'sv_submission_id',
        allowNull: false
    },
    sv_value: {
        type: Sequelize.INTEGER(1),
        field: 'sv_value',
        allowNull: false
    }
});

/* Methods for submission model */
SubmissionVote.methods = {
    createTableIfNotExists: function() {
        SubmissionVote.sync();
    },
        
    getKarmaForPost: function(submissionId) {
        var sql = "SELECT SUM(sv_value) FROM SUBMISSION_VOTES WHERE SV_SUBMISSION_ID=" + submissionId;
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then (function(result) {
            console.log(result[0]['SUM(sv_value)']);
        })
    }
};

module.exports = SubmissionVote;