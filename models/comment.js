/*  |===================================
    |COMMENT MODEL======================
    |===================================
    |com_id                             
    |com_parent_id                      
    |com_commenter_id                   
    |com_submission_id
    |com_text
    |=================================== */

/* Required packages */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

/* Define Comment model */
var Comment = sequelize.define('comment', {
    com_commenter_id: {
        type: Sequelize.INTEGER(8),
        field: 'com_commenter_id',
        allowNull: false
    },
    com_submission_id: {
        type: Sequelize.INTEGER(8),
        field: 'com_submission_id',
        allowNull: false
    },
    com_parent_comment_id: {
        type: Sequelize.INTEGER(8),
        field: 'com_parent_comment_id',
        defaultValue: 0,
        allowNull: false
    },
    com_text: {
        type: Sequelize.STRING(255),
        field: 'com_text',
        allowNull: false
    },
    com_karma: {
        type: Sequelize.INTEGER(8),
        field: 'com_karma',
        allowNull: false,
        defaultValue: 0
    }
});

/* Methods for comment model */
Comment.methods = {
    createTableIfNotExists: function() {
        Comment.sync();
    },
    
    addComment: function(submissionId, parentId, commenterId, text) {
        console.log("Adding comment...");
        if (parentId == 0) {
            return Comment.create({
                com_submission_id: submissionId,
                com_parent_id: parentId,
                com_commenter_id: commenterId,
                com_text: text
            }).then (function() {
                console.log("Comment created");     
            }).catch (function(err) {
                console.log(err);
            });
        } else {
            return Comment.create({
                com_commenter_id: commenterId,
                com_submission_id: submissionId,
                com_parent_id: parentId,
                com_parent_comment_id: parentId,
                com_text: text
            }).then (function() {
                console.log("Adding comment success.");
            }).catch (function(err) {
                console.log(err);
                console.log("Adding comment failed."); 
            });
            console.log("DONE");
        }
    },
    
    editComment: function(commentId, text) {
        console.log("Changing existing comment...");
        Comment.update({
            com_text: text  
        }, { where: { 
            id: commentId 
        }}).then (function() {
            console.log("Comment edit success."); 
        }).catch (function(err) {
            console.log("Comment edit failed.");
            console.log(err);
        });
        console.log("DONE");
    },
    
    removeComment: function(commentId) {
        console.log("Deleting comment...");
        Comment.destroy({
            where: {
                id: commentId
            }
        }).then (function() {
            console.log("Comment deletion success."); 
        }).catch (function(err) {
            console.log("Comment deletion failed.");
            console.log(err);
        });
        console.log("DONE");
    },
        
    getComments: function(submissionId, callback) {
        var sql = "SELECT * FROM COMMENTS WHERE com_submission_id = " + submissionId;
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then (function(result) {
            console.log(result); 
        });
    },
}

module.exports = Comment;