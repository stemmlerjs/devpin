/*  |===================================
    |COMMENT VOTE MODEL=================
    |===================================
    |vc_voter_id                      
    |vc_comment_id                    
    |vc_value                       
    |=================================== */

/* Required packages and files */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

/* Define Comment Vote model */
var CommentVote = sequelize.define('comment_vote', {
    vc_voter_id: {
        type: Sequelize.INTEGER(8),
        field: 'vc_voter_id',
        allowNull: false
    },
    vc_comment_id: {
        type: Sequelize.INTEGER(8),
        field: 'vc_comment_id',
        allowNull: false
    },
    vc_value: {
        type: Sequelize.INTEGER(1),
        field: 'vc_value',
        defaultValue: 0,
        allowNull: false
    }
});

/* Methods for vote comment model */
CommentVote.methods = {
    createTableIfNotExists: function() {
        CommentVote.sync();
    },
    
    addUpVote: function(commentId, userId) {
        // ####################
        // TODO: USER CANNOT UPVOTE IF UPVOTE EXISTS
        // ####################
        console.log("Adding up vote to comment...");
        CommentVote.findOrCreate({
            where: {
                id: commentId,
                vc_voter_id: userId
            },
            defaults: {
                vc_voter_id: userId,
                vc_comment_id: commentId,
                vc_value: 1
            }
        }).then (function(vote) {
            CommentVote.update({
                vc_value: 1
            }, { where: {
                id: commentId,
                vc_voter_id: userId
            }}).then (function() {
                console.log("Vote update success");
            }).catch (function(err) {
                console.log("Vote update failed."); 
            });
        }).catch (function(err) {
            console.log("Adding new up vote failed");
            console.log(err);
        });
    },
    
    addDownVote: function(commentId, userId) {
        console.log("Adding down vote to comment...");
        // ####################
        // TODO: USER CANNOT DOWN VOTE IF DOWN VOTE EXISTS
        // ####################
        CommentVote.findOrCreate({
            where: {
                id: commentId,
                vc_voter_id: userId
            },
            defaults: {
                vc_voter_id: userId,
                vc_comment_id: commentId,
                vc_value: -1
            }
        }).then (function() {
            CommentVote.update({
                vc_value: -1
            }, { where: {
                id: commentId,
                vc_voter_id: userId
            }}).then (function() {
                console.log("Vote update success");
            }).catch (function(err) {
                console.log("Vote update failed."); 
            });
        }).catch (function() {
            console.log("Adding new down vote failed");
        });
    },
    
    removeVote: function(commentId) {
        console.log("Removing vote");
        CommentVote.destroy({
            where: {
                id: commentId
            }
        }).then (function() {
            console.log("Vote delete success.");
        }).catch (function(err) {
            console.log("Vote delete failed.");
            console.log(err);
        });
        console.log("DONE");
    },
    getVoterId: function() {
        return this.vc_voter_id;
    },
    getSubmissionId: function() {
        return this.vc_submission_id;
    },
    getVoteValue: function() {
        return this.vc_value;
    }
}

module.exports = CommentVote;