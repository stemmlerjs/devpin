/*  |===================================
    |USER TAG MODEL==========================
    |===================================
    |id                             
    |ut_user_id
    |ut_tag
    |==================================== */

/* Required packages */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

/* Define Submission model */
var UserTag = sequelize.define('user_tag', {
    ut_user_id: {
        type: Sequelize.INTEGER(25),
        field: 'ut_user_id',
        allowNull: false
    },
    ut_tag: {
        type: Sequelize.STRING(25),
        field: 'ut_tag',
        allowNull: false
    }
});

/* Methods for submission model */
UserTag.methods = {
    createTableIfNotExists: function() {
        UserTag.sync();
    },
    
    addUserTag: function(userId, title) {
        console.log("Creating new user tag...");
        var sql = "SELECT * FROM USER_TAGS WHERE UT_TAG='" + title + "'";
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then (function(result) {
            if (result[0]) console.log("User tag already exists.");
            else {
                return UserTag.create({
                    ut_user_id: userId,
                    ut_tag: title
                }).then (function() {
                    console.log("User tag generation success");
                }).catch (function(err) {
                    console.log("user tag generation failed.");
                    console.log(err);
                });  
            }
        });
    },
    
    removeUserTag: function(userId, title) {
        console.log("Removing tag...");
        UserTag.destroy({
            where: {
                ut_user_id: userId,
                ut_tag: title
            }
        }).then (function() {
            console.log("user tag removal success");
        }).catch (function(err){
            console.log("user tag removal failed");
            console.log(err);
        });
        console.log("DONE");
    },
};

module.exports = UserTag;