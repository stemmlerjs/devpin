<<<<<<< HEAD
*  |===================================
=======
/*  |===================================
>>>>>>> 857b5414cba6a1f20c1782035b39ce8f758a96f6
    |SUBMISSION TAG MODEL=============
    |===================================
    |id
    |st_user_id
    |st_submission_id
    |st_category                                        
    |==================================== */

/* Required packages */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;
<<<<<<< HEAD
var lodash = require('lodash');
=======
var _ = require('lodash');
var Submission = require('./submission.js');
>>>>>>> 857b5414cba6a1f20c1782035b39ce8f758a96f6

/* Define Submission Tag model */
var SubmissionTag = sequelize.define('submission_tag', {
    st_submission_id: {
        type: Sequelize.INTEGER(8),
        field: 'st_submission_id',
        allowNull: false
    },
    st_category: {
        type: Sequelize.STRING(25),
        field: 'st_tag_id',
        allowNull: false
    }
});

/* Methods for submission model */
SubmissionTag.methods = {
    createTableIfNotExists: function() {
        SubmissionTag.sync();
    },
    
    addSubmissionTag: function(submissionId, tag) {
        console.log("Creating new submission tag...");
        return SubmissionTag.create({
            st_submission_id: submissionId,
            st_category: tag
        }).then (function() {
            console.log("Submission tag creation success"); 
        }).catch (function(err) {
            console.log("Submission tag creation failed");
            console.log(err);
        });
        console.log("DONE");
    },
    
    removeSubmissionTag: function(submissionTagId) {
        console.log("Removing submission tag...");
        SubmissionTag.destroy({
            where: {
                id: submissionTagId
            }
        }).then (function() {
            console.log("Submission tag deletion success."); 
        }).catch (function(err) {
            console.log("Submission tag deletion failed.");
            console.log(err);
        });
        console.log("DONE");
    },
<<<<<<< HEAD
    
    getAllSubmissionsByTag: function(tags, callback){
        var sql = "SELECT DISTINCT ST_SUBMISSION_ID FROM SUBMISSION_TAGS ";

        for(var i = 0; i < tags.length; i++){
            if(i == 0){
                sql += " WHERE ST_TAG_ID = '" + tags[i] + "' ";
            } else {
                sql += "OR ST_TAG_ID = '" + tags[i] + "' ";
            }
        }
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then(function(result){
            var submission_ids = [];
            for(var j = 0; j < result.length; j++){
                submission_ids.push(result[j].ST_SUBMISSION_ID);
            }

            sql = "SELECT * FROM SUBMISSIONS ";

            for(var h = 0; h < submission_ids; h++){
                if(h == 0){
                    sql += "WHERE ID = " + submission_ids[h] + " ";
                } else {
                    sql += "OR ID = " + submission_ids[h] + " ";
                }
            }
        });
    },
    
    getSubmissionsByDate: function(postLimit, callback) {
        var retData = {};
        var request = [];
        
        var sql = "SELECT * FROM SUBMISSIONS WHERE UPDATEDAT < CURRENT_TIMESTAMP ORDER BY UPDATEDAT DESC LIMIT " + postLimit;
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then(function(result) {
            result.forEach(function(res){
                    var promise = new Promise(function(resolve){
                        SubmissionTag.find({
                        where: {
                            st_submission_id: res.id
                        }
                    }).then(function(stags){
                        console.log("-----------> Data ");
                        console.log(stags);
                        if(stags){
                            result['tags'] = stags;

                        //Add to the return object
                        retData[result.id] = result; 
                        }
                        resolve();

                    });
                });
                request.push(promise);
                console.log("here");
            });
            
            Promise.all(request).then(function(values){
                console.log("now we're here");
                callback(retData);
            });
      });
  },
  
  // ####
  // TODO: ADD SUBSCRIPTION TO USERS
    // ####
    getCurratedList: function(postLimit, userId, callback) {
        var sql = "SELECT * FROM USER_TAGS WHERE UT_USER_ID='" + userId + "'";
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then (function(result) {
            var sqlInner = "SELECT * FROM SUBMISSIONS WHERE UPDATEAT < CURRENT_TIMESTAMP";
            for (var i = 0; i < result.length; i++) {
                sqlInner = sqlInner + " AND UT_TAG='" + result[i].ut_tag + "'";
            }
            sqlInner = sqlInner + " ORDER BY UPDATEAT ASC LIMIT " + postLimit;
            sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then (function(result2) {
                console.log(result2); 
            });
        });
=======

    getAllSubmission: function(callback){
        var retData = {};
        var request = [];
        Submission.findAll({}).then(function(submissions){
            submissions.forEach(function(subInSub){
                console.log("the user id for submission ' " + subInSub.dataValues.sub_title + " ' is "  + subInSub.dataValues.sub_user_id);
                // Add the Post to the Object to pass back
                retData[subInSub.dataValues.id] = subInSub.dataValues;
                // Get the User
                var promise = new Promise(function(resolve) {
                    Submission.methods.getUserFromSubmissionId(subInSub.dataValues.sub_user_id, function (user) {
                        console.log(user.user_name);
                        console.log("The user for " + subInSub.dataValues.sub_user_id + " sub_user_id is " + user.user_name + " with the id of: " + user.id);
                        retData[subInSub.dataValues.id.user] = user;
                        resolve();
                    });
                });
                request.push(promise);
            });

            Promise.all(request).then(function(values){
                console.log("promise!!!!");
                callback(retData);
            });

        });
    },
    
    getAllSubmissionsByTag: function(tags, limit, offset, callback) {
        //If there are no tags selected, return null
        if (tags.length > 0) {
            var pre_intersection = [];

            // Get each
            tags.forEach(function(tag){
                var obj = {
                    st_category: tag
                };
                pre_intersection.push(obj);
            });

            // This is the full query, now we need to find the ones that have
            //st_submission_id for each tag
            SubmissionTag.findAll({
                where: {
                    $or: pre_intersection
                }
            }).then(function(result){
                var almostFinal = [];
                for(var h = 0; h < result.length; h++){
                    almostFinal.push(result[h].dataValues);
                }

                // Algorithm
                var intersection = {};
                almostFinal.forEach(function(element){
                    // Not in the intersection left
                    if(!intersection[element.st_submission_id]){
                        intersection[element.st_submission_id] = {
                            count: 1,
                            submission: element
                        }
                    } else {
                        //Iterate the count forward
                        var temp = intersection[element.st_submission_id];
                        temp.count++;
                        intersection[element.st_submission_id] = temp;
                    }
                });

                // Now, only include the selected ones
                var filteredSubmissions = [];
                var request = [];

                console.log("intersection_____________________>");
                console.log(intersection);
                _.forEach(intersection, function(value, key){
                    if(value.count === tags.length){
                        //Get other things that we need
                        var promise = new Promise(function(resolve){
                            Submission.methods.getUserFromSubmissionId(value.submission.st_submission_id, function(user){
                                var retData = value.submission;
                                console.log("Get other things we need: ---------------- Submission BEFORE");
                                console.log(retData);
                                retData['user'] = user;
                                retData['tags'] = tags;
                                console.log("Submission OBJECT AFTER ---------------");
                                console.log(retData);
                                filteredSubmissions.push(retData);
                                resolve();
                            });
                        });
                        request.push(promise);
                    }
                });

                Promise.all(request).then(function(values){
                    console.log("promise!!!!");
                    callback(filteredSubmissions);
                });

            });

        } else {
            callback(null);
        }
>>>>>>> 857b5414cba6a1f20c1782035b39ce8f758a96f6
    }
};

module.exports = SubmissionTag;