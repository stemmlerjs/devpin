  |===================================
    |SUBMISSION MODEL===================
    |===================================
    |sub_id
    |sub_title
    |sub_user_id
    |sub_link
   |=================================== */

/* Required packages and files */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;
var SubmissionVote = require('../models/submission_vote.js')
var Tag = require('../models/tag.js')

/* Define submission model */
var Submission = sequelize.define('submission', {
    sub_title: {
        type: Sequelize.STRING(50),
        field: 'sub_title',
        allowNull: false
    },
    sub_user_id: {
        type: Sequelize.INTEGER(8),
        field: 'sub_user_id',
        allowNull: false
    },
    sub_link: {
        type: Sequelize.STRING(255),
        field: 'sub_link',
        allowNull: false
    }
});

/* Methods for submission model */
Submission.methods = {
   createTableIfNotExists: function() {
        Submission.sync();
    },
    
    createSubmission: function(title, userId, link, tags) {
       console.log("Generating specific submission id...");        
        console.log("Adding new submission...");
       for (var i = 0; i < tags.length; i++) {
            Tag.findOrCreate({
                where: {
                    tag_title: tags[i]    
                }
            });
        }
        return Submission.create({
            sub_title: title,
            sub_user_id: userId,
            sub_link: link
        }).then (function () {
            console.log("New submission successfully created...");
        }).catch (function () {
            console.log("New submission failed."); 
        });
        
        console.log("DONE");
    },
    
    removeSubmission: function(id) {
        console.log("Removing specific submission...");
        console.log("Searching for matching post...");
        Submission.destroy({
            where: {
                id: id
            }
        }).then (function() {
            console.log("Submission deleted.");
        }).catch (function() {
           console.log("Submission deletion failed...");
       });
        console.log("DONE");
    },
    
    upVotePost: function(userId, submissionId) {
        SubmissionVote.findOrCreate({
            where: {
                sv_voter_id: userId,
                sv_submission_id: submissionId,
                sv_value: 1
            },
            defaults: {
                sv_voter_id: userId,
                sv_submission_id: submissionId,
                sv_value: 1
            }
        }).then (function() {
            console.log("created a new vote");
        }).catch (function() {
            console.log("vote already exists");
        });
    },
    
    downVotePost: function(userId, submissionId) {
        SubmissionVote.findOrCreate({
            where: {
               sv_voter_id: userId,
                sv_submission_id: submissionId,
                sv_value: -1
            }, 
            defaults: {
                sv_voter_id: userId,
                sv_submission_id: submissionId,
                sv_value: -1
            }
        }).then (function() {
            console.log("created a new down vote");            
        }).catch (function() {
            console.log("failed down vote");
        });
    }
};

module.exports = Submission;