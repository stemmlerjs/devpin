/*  |===================================
    |TAG MODEL==========================
    |===================================
    |id                             
    |tag_title                          
    |==================================== */

/* Required packages */
var Sequelize = require('sequelize');
var sequelize = require('../config/config.js').dbConnection;

/* Define Submission model */
var Tag = sequelize.define('tag', {
    tag_title: {
        type: Sequelize.STRING(50),
        field: 'tag_title',
        allowNull: false
    }
});

/* Methods for submission model */
Tag.methods = {
    createTableIfNotExists: function() {
        Tag.sync();
    },
    
    addTag: function(title) {
        console.log("Creating new tag...");
        var sql = "SELECT * FROM TAGS WHERE TAG_TITLE='" + title + "'";
        sequelize.query(sql, { type: sequelize.QueryTypes.SELECT }).then (function(result) {
            if (result[0]) console.log("tag already exists.");
            else {
                return Tag.create({
                    tag_title: title
                }).then (function() {
                    console.log("Tag generation success");
                }).catch (function(err) {
                    console.log("Tag generation failed.");
                    console.log(err);
                });  
            }
        });
    },
    
    removeTag: function(tagId) {
        console.log("Removing tag...");
        Tag.destroy({
            where: {
                id: tagId
            }
        }).then (function() {
            console.log("Tag removal success");
        }).catch (function(err){
            console.log("Tag removal failed");
            console.log(err);
        });
        console.log("DONE");
    },
    
    getId: function() {
        return this.tag_id;
    },
    
    getTitle: function() {
        return this.tag_title;
    }
};

module.exports = Tag;