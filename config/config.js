
// ========= Get Required Packages
var Sequelize   = require('sequelize');              // database management modeling system module

var db = new Sequelize('devpin', 'root', process.env.DB_PASSWORD, {
    host: 'localhost',
    dialect: 'mysql',
    logging: false,
    pool: {
        max: 15,
        min: 0,
        idle: 10000
    }
});

var secret = "thebirthdaypartyismyfavouriteband";

var runningConfig = {
    dbConnection: db,
    secret: secret
};



module.exports = runningConfig;