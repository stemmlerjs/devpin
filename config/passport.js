
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

// load up the user model
var User            = require('../models/user.js');

// load the auth variables
var configAuth = require('./auth');
module.exports = function(passport) {

// ========== LOCAL STRATEGY ============
// ======================================

    // used to serialize the user for the session
    passport.serializeUser(function(user, done){
        done(null, user.id); //callback for what to do next
    });

    // used to deserialize the user from the session
    passport.deserializeUser(function(id, done){ //pass id to User model
        User.findOne({
            where: {
                id: id
            }
        }).then(function(user){
            done(user);
        }).catch(function (err) {
            console.log("An error occured deserializing a user---->   " + err);
        });
    });

    // =========================================================================
    // LOCAL SIGNUP ============================================================
    // =========================================================================

    passport.use('local-signup', new LocalStrategy({
            //by default, local strategy uses username and password,
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true //allows us to pass back the entire request to the callback
        },
        function (req, username, password, done) {

            process.nextTick(function () {

                User.findOne({
                    where: {
                        user_name: username
                    }
                }).then(function (user) {
                    if (user) {
                        console.log("The username: " + username + " is already taken.");
                        return done(null, false, req.flash('signupMessage', 'That username is already taken.'));

                    } else {
                        // If username isn't already taken, then create account

                        User.methods.createAccount(username, password, req.body.email, function(newUser){
                            console.log("Success creating user");
                            return done(null, newUser);
                        });
                    }
                });
            });
        }
    ));


    // =========================================================================
    // LOCAL LOGIN =============================================================
    // =========================================================================

        passport.use('local-login', new LocalStrategy({
            // by default, local strategy uses username and password, we will override with email and pass
            usernameField: 'username',
            passwordField: 'password',
            passReqToCallback: true
        }, function (req, username, password, done) {

            User.find({
                where: {
                    user_name: username
                }
            }).then(function(user){

                if(user){
                    // If the user exists, check if the password is correct
                    user.comparePassword(password, function(err, res){

                        //If password matches
                        if(res){
                            // Log the user in
                            console.log("passwords match");
                            return done(null, user);
                        } else {
                            //Passwords don't match
                            console.log("passwords don't match");
                            return done(null, false, req.flash('loginMessage', 'Either username or password is incorrect.'));
                        }
                    });
                } else {
                    //If the user doesn't exist, send a flash message
                    console.log("user doesnt exist");
                    return done(null, false, req.flash('loginMessage', 'Either username or password is incorrect.'));
                }

            }).catch(function(err){
               console.log(err);
                return done(null, false);
            });

    }));

};



