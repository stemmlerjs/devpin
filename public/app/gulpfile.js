var gulp = require('gulp'),
    browserify = require('browserify'),
    source = require('vinyl-source-stream'),
    buffer = require('vinyl-buffer'),
    reactify = require('reactify'),
    babelify = require('babelify'),
    sourcemaps = require('gulp-sourcemaps'),
    eslint = require('gulp-eslint'),
    sass = require('gulp-sass'),
    cssnano = require('gulp-cssnano')
    source = require('vinyl-source-stream'),
    concat = require('gulp-concat'),
    es = require('event-stream');


gulp.task('buildjs', function() {
  browserify({
    entries: ['./src/js/main.js'],
    transform: [babelify.configure({
      optional: ['es7.classProperties', 'es7.decorators']
    }), reactify],
    debug: true,
    paths: ['./src/js'],
  })
  .bundle()
  .pipe(source('main.js'))
  .pipe(buffer())
  .pipe(sourcemaps.init({loadMaps: true}))
  .pipe(sourcemaps.write('./'))
  .pipe(gulp.dest('dist'));
});

gulp.task('eslint', function() { 
  return gulp.src('./src/js/**/*.js')
    .pipe(eslint())
    .pipe(eslint.format())
    .pipe(eslint.failAfterError());
});

gulp.task('style', function() {
  var srcSass = gulp.src('./src/sass/**/*.scss')
    .pipe(sass().on('error', sass.logError)); 
  var vendor = gulp.src('./src/sass/vendors/bootstrap/bootstrap.css'); 

  return es.concat(vendor, srcSass)
    .pipe(concat('style.css'))
    .pipe(gulp.dest('dist/css'));
});

gulp.task('move static', function() {
  var filesToMove = [
    './src/fonts/**/*.*',
    './src/images/*.*'
  ]

  return gulp.src(filesToMove, { base: './src' })
    .pipe(gulp.dest('dist'));
})

gulp.task('default', ['eslint', 'buildjs', 'style', 'move static'], function() {
  return gulp.watch('src/**/*.*', ['eslint', 'buildjs', 'style', 'move static']);
});
