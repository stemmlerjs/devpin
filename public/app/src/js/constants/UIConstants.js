export const MODAL_EMPTY = 'MODAL_EMPTY';
export const MODAL_LOGIN = 'MODAL_LOGIN';
export const MODAL_REGISTER = 'MODAL_REGISTER';

export const USER_LOGIN = 'USER_LOGIN';
export const USER_REGISTER = 'USER_REGISTER';
export const CLOSE_MODAL = 'CLOSE_MODAL';