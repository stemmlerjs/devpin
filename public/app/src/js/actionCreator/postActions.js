import { RECEIVED_POSTS } from '../constants/postConstants';
import { postResource } from '../utils/postUtil';

export function fetchPosts(tags = []) {
  return (dispatch) => {
    postResource.getCollection(tags)
    .then((payload) => {
      dispatch(fetchPostsSucceeded(payload.posts));
    });
  };
}

export function createPost(title, link, tags) {
  return (dispatch) => { 
    
  };
}

function fetchPostsSucceeded(posts) { 
  return {
    type: RECEIVED_POSTS, 
    posts: posts
  };
}
