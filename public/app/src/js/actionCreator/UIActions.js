import {USER_LOGIN, USER_REGISTER, CLOSE_MODAL} from '../constants/UIConstants';

export function openLoginModal() {
	return { type: USER_LOGIN };
}

export function openRegisterModal() {
	return { type: USER_REGISTER };
}

export function closeModal() {
	return { type: CLOSE_MODAL };
}