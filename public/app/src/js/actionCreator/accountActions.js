import { LOGIN_SUCCEEDED, SIGNUP_SUCCEEDED } from '../constants/accountConstants';
import { decodeJWT, signupResource, loginResource } from '../utils/authUtil';

export function signup(email, username, password) {
  return (dispatch) => { 
    signupResource.signup(email, username, password)
    .then((payload) => {
      // successfully logged in 
      const jwt = payload.token;
      const user = decodeJWT(jwt);
      // save the jwt into local storage
      localStorage.setItem('jwt', jwt);
      dispatch(signupSucceeded(user, jwt));
    });
  };
}

export function login(email, username, password) {
  return (dispatch) => {
    loginResource.login(email, username, password)
    .then((payload) => {
      // successfully logged in 
      const jwt = payload.token;
      const user = decodeJWT(jwt);
      // save the jwt to local storage
      localStorage.setItem('jwt', jwt);
      dispatch(loginSucceeded(user, jwt));
    });
  };
}

function signupSucceeded(user, jwt) { 
  return { 
    type: SIGNUP_SUCCEEDED,
    user: user,
    jwt: jwt
  };
}

function loginSucceeded(user, jwt) {
	return {
		type: LOGIN_SUCCEEDED,
		user: user,
		jwt: jwt
	};
}

