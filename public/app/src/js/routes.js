import React from 'react';
import { Router, Route } from 'react-router';
import { ReduxRouter } from 'redux-router';
import Root from './components/root';
import Home from './components/home';
import NewSubmission from './components/newSubmission';

const routes = (
  <ReduxRouter> 
    <Route path='/' component={Root}>
      <Route path='home' component={Home}></Route>
      <Route path='new-submission' component={NewSubmission}></Route>
    </Route>
  </ReduxRouter> 
);

export default routes;
