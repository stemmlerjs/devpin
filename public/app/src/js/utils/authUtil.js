const signupURI = '/signup';
const authURI = 'api/auth';

function checkStatus(res) { 
  if (res.status >= 500) {
    throw new Error('Server Error');
  }

  if (res.status >= 400) {
    throw new Error('Username or Password is Incorrect');
  }

  return res.json();
}

export const signupResource = { 
  signup: (email, username, password) => { 
    return fetch(signupURI, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        email: email,
        username: username,
        password: password
      })
    })
    .then(checkStatus);
  }
};

export function decodeJWT(jwt) {
  // the jwt is a dot seperated data entity, the 2nd entity is an base64 encoded user
  // object
  return JSON.parse(atob(jwt.split('.')[1]));
}

export const loginResource = { 
  login: (username, password) => {
    return fetch(authURI, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        username: username, 
        password: password
      })
    })
    .then(checkStatus);
  }
};
