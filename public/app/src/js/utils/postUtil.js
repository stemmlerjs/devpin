const postsURI = '/api/posts';

function checkStatus(res) {
  if (res.status >= 400) {
    throw new Error('Something went wrong');
  }

  return res.json();
}


export const postResource = {
  getCollection: (tags = []) => { 
    const queryParams = `encodedTags=${btoa(JSON.stringify(tags))}`;
    return fetch(`${postURI}?${queryParams}`, {
      method: 'get',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    })
    .then(checkStatus);
  },
  post: (title, link, tags) => {
    return fetch(postsURI, {
      method: 'post',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ 
        title: title,
        link: link,
        tags: tags
      })
    })
    .then(checkStatus);
  }
};
