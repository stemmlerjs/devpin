import { createStore, compose } from 'redux';
import rootReducer from '../reducers/appReducer';
import { reduxReactRouter } from 'redux-router';
import { createHistory } from 'history';
import routes from '../routes';
import thunkMiddleware from 'redux-thunk';
import { applyMiddleware } from 'redux';

const createStoreWithMiddleware = compose(
  applyMiddleware(
    thunkMiddleware
  ),
  reduxReactRouter({
    routes,
    createHistory
  })
)(createStore);

const store = createStoreWithMiddleware(rootReducer);

export default store;
