import React, { Component, PropTypes } from 'react';

class PostItem extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { post } = this.props;

    const postTagsJSX = post.tags.map((tag) => <div>{tag}<span className='glyphicon glyphicon-plus'></span></div>);
    return (
      <div className='submission-item'> 
        <div className='votes-container'>
          <span className='glyphicon glyphicon-triangle-top'></span>
          <span>{post.votes}</span>
          <span className='glyphicon glyphicon-triangle-bottom'></span>
        </div>
        <a className='post-title' href={post.link}>{ post.title }</a>
        <a className='post-author' href={post.link}>{ post.author }</a>
        <div className='comments-container'>
          <span className='glyphicon glyphicon-comment'></span>
          <span>{post.comments}</span>
        </div>
        <div className='post-tags'>
          {postTagsJSX}
        </div>
      </div>
    );
  }
}
export default PostItem;
