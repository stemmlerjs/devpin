import React, { Component, PropTypes } from 'react';

import LoginRegisterBtnGroup from './loginRegisterBtnGroup';
import UserInfoBtnGroup from './userInfoBtnGroup';

class navbar extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const openLoginModal = this.props.openLoginModal;
    const openRegisterModal = this.props.openRegisterModal;
    var content;

    if (this.props.user.user) {
        content = (<UserInfoBtnGroup />);
    } else {
        content = (<LoginRegisterBtnGroup
                openLoginModal={openLoginModal} 
                openRegisterModal={openRegisterModal}/>);
    }

    return (
        <div className='navbar'>
            <a href='/home'><img src='app/dist/images/Logo.png' alt='DevPin' /></a>
            {content}
        </div>
    );
  }
}

export default navbar;
