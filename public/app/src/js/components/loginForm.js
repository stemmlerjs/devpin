import React, { Component, PropTypes } from 'react';
import Input from 'react-bootstrap/lib/Input';
import ButtonInput from 'react-bootstrap/lib/ButtonInput';

class loginForm extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      password: ''
    };
  }

  componentDidMount() {}

  handleUsernameChange(e) {
    this.setState({
      username: e.target.value
    });
  }

  handlePasswordChange(e) {
    this.setState({
      password: e.target.value
    });
  }

  completeLogin() {
    const { username, password } = this.state;
    this.props.login(username, password);
  }

  render() {
    return (
        <div>
          <Input type='text' placeholder='Username...' onChange={this.handleUsernameChange.bind(this)} />
          <Input type='password' placeholder='Password...' onChange={this.handlePasswordChange.bind(this)} />
          <ButtonInput className='btn-success' type='submit' value='Log In' onClick={this.completeLogin.bind(this)}/>
        </div>
    );
  }
}

export default loginForm;
