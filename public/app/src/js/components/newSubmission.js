import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import Input from 'react-bootstrap/lib/Input';
import ButtonInput from 'react-bootstrap/lib/ButtonInput';

class newSubmission extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return(
      <div className='new-post-container'>
        <h1>New Post</h1>
        <Input type='text' placeholder='Title' />
        <Input type='text' placeholder='Tags...' />
        <Input type='text' placeholder='Link/URL' />
        <ButtonInput className='btn-success' type='submit' value='Submit' onClick={(text) => alert(text)} />
      </div>
    );
  }
}

function select(state) {
  return {
    q: state.router.location.query.q,
    account: state.account
  };
}

export default connect(select)(newSubmission);