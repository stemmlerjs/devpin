import React, { Component, PropTypes } from 'react';
import _ from 'lodash';
import PostItem from './postItem';


function generatePostItemsJSX(posts) {
  // posts is a object of objects with the keys as the object ids
  // convert to a list 
  return _.map(posts, (post) => { 
    return <PostItem post={post} key={post.id} className="submission-item"/>;
  });
}

class PostList extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { posts } = this.props;
    const postItemsJSX = generatePostItemsJSX(posts);
    return (
      <div className='inner-content-container'> 
       { postItemsJSX }
      </div>
    );
  }
}

export default PostList;
