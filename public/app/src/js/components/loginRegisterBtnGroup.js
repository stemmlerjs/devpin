import React, { Component, PropTypes } from 'react';

import Button from 'react-bootstrap/lib/Button';

class LoginRegisterBtnGroup extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const openLoginModal = this.props.openLoginModal;
    const openRegisterModal = this.props.openRegisterModal;
    
    return(
      <div className='btn-container'>
        <Button 
            bsStyle='primary' 
            className='log-in' 
            onClick={openLoginModal.bind(this, 'login')}
        >Log In</Button>
        <Button 
            bsStyle='primary' 
            className='register' 
            onClick={openRegisterModal.bind(this, 'register')}
        >Register</Button>
      </div>
    );
  }
}

export default LoginRegisterBtnGroup;