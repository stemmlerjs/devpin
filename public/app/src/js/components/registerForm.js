import React, { Component, PropTypes } from 'react';
import Input from 'react-bootstrap/lib/Input';
import ButtonInput from 'react-bootstrap/lib/ButtonInput';

class registerForm extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {
      username: '',
      email: '',
      password1: '',
      password2: ''
    };
  }

  handleChange(attr, e) { 
    let state = {};
    state[attr] = e.target.value;
    this.setState(state);
  }

  completeRegistration() { 
    const { email, username, password1, password2 } = this.state;
    if (password1 === password2) { 
      this.props.register(email, username, password1);
    }
  }

  componentDidMount() {}

  render() {
    return (
        <div>
            <Input type='text' placeholder='Username...' onChange={this.handleChange.bind(this, 'username')} />
            <Input type='email' placeholder='Email...' onChange={this.handleChange.bind(this, 'email')}/>
            <Input type='password' placeholder='Password...' onChange={this.handleChange.bind(this, 'password1')}/>
            <Input type='password' placeholder='Confirm Password...' 
              onChange={this.handleChange.bind(this, 'password2')}/>
            <ButtonInput className='btn-success' type='submit' value='Register' onClick={this.completeRegistration.bind(this)}/>
        </div>
    );
  }
}

export default registerForm;
