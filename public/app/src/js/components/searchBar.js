import React, { Component, PropTypes } from 'react';

class searchBar extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    return (
        <div className='searchbar-container'>
            <input type='text' className='search-input' placeholder='Search for topics, tags, authors...' />
            <input type='submit' className='search-submit' value='Search' />
        </div>
    );
  }
}

export default searchBar;
