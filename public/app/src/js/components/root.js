import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { openLoginModal, openRegisterModal, closeModal } from '../actionCreator/UIActions';
import { MODAL_EMPTY, MODAL_LOGIN, MODAL_REGISTER } from '../constants/UIConstants';
import { signup, login } from '../actionCreator/accountActions';

import Navbar from './navbar'; 
import HomeModal from './homeModal';

class root extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { dispatch, UI, account } = this.props;

    var showModal = false,
        activeKey = 1;

    if (UI.modalState !== MODAL_EMPTY) {
        showModal = true;
        if (UI.modalState !== MODAL_LOGIN) {
            activeKey = 2;
        }
    }

    return (
    <div className='root-container'>
        <Navbar 
            openLoginModal={() => dispatch(openLoginModal())} 
            openRegisterModal={() => dispatch(openRegisterModal())}
            user={account}
        />
        { this.props.children }
        <HomeModal 
            showModal={showModal}
            activeKey={activeKey}
            openLoginModal={() => dispatch(openLoginModal())} 
            openRegisterModal={() => dispatch(openRegisterModal())}
            closeModal={() => dispatch(closeModal())}
            register={(email, username, password) => dispatch(signup(email, username, password))}
            login={(username, password) => dispatch(login(username, password))}
        />
    </div>
    );
  }
}

function select(state) {
  return {
    q: state.router.location.query.q,
    UI: state.UI,
    account: state.account
  };
}


export default connect(select)(root);
