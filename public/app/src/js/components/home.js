import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';

import SearchBar from './searchBar'; 
import PostList from './postList';

class home extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  render() {
    const { dispatch, posts } = this.props;

    return (
    <div className='home-container'>
        <SearchBar />
        <PostList posts={posts} />
    </div>
    );
  }
}

function select(state) {
  return {
    q: state.router.location.query.q,
    posts: state.posts
  };
}


export default connect(select)(home);
