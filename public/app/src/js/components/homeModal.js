import React, { Component, PropTypes } from 'react';
import LoginForm from './loginForm';
import RegisterForm from './registerForm';
import Modal from 'react-bootstrap/lib/Modal';
import Tabs from 'react-bootstrap/lib/Tabs';
import Tab from 'react-bootstrap/lib/Tab';


class homeModal extends Component {
  static PropType = {};

  constructor(props) {
    super(props);
    this.state = {};
  }

  componentDidMount() {}

  handleSelect(selectedKey) {
    if (selectedKey === 1) {
      this.props.openLoginModal();
    } else {
      this.props.openRegisterModal();
    }
  }

  render() {
    const { closeModal, register, login } = this.props;
    return (
      <div>
        <Modal show={this.props.showModal} onHide={closeModal}>
            <Modal.Header closeButton>
            </Modal.Header>
            <Modal.Body closeButton>
                <Tabs 
                      bsStyle='pills' 
                      activeKey={this.props.activeKey} 
                      position='top' 
                      onSelect={this.handleSelect.bind(this)}
                >
                      <Tab eventKey={1} title='Log In'>
                          <LoginForm 
                            login={login}
                          />
                      </Tab>
                      <Tab eventKey={2} title='Register'>
                          <RegisterForm 
                            register={register}
                          />
                      </Tab>
                </Tabs>
            </Modal.Body>
        </Modal>
      </div>
    );
  }
}

export default homeModal;
