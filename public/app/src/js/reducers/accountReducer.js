import { LOGIN_SUCCEEDED, SIGNUP_SUCCEEDED} from '../constants/accountConstants';
import { decodeJWT } from '../utils/authUtil';

function defaultState() {
  let jwt = localStorage.getItem('jwt'); 
  return {
    user: jwt ? decodeJWT(jwt) : null,
    jwt: jwt || ''
  };
}

export default function(state=defaultState(), action) {
	switch(action.type) {
    case SIGNUP_SUCCEEDED:
		case LOGIN_SUCCEEDED:
			return { ...state, user: action.user, jwt: action.jwt};
	}
	return state;
}
