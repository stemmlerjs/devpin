import * as UIConstants from '../constants/UIConstants';

const defaultState = { 
  modalState: UIConstants.MODAL_EMPTY
};

export default function(state=defaultState, action) {
  switch(action.type) {
    case UIConstants.USER_LOGIN:
      return { ...state, modalState: UIConstants.MODAL_LOGIN };
    case UIConstants.USER_REGISTER:
      return { ...state, modalState: UIConstants.MODAL_REGISTER };
    default:
      return { ...state, modalState: UIConstants.MODAL_EMPTY };
  }

  return state;
}