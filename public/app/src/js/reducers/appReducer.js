import { combineReducers } from 'redux';
import { routerStateReducer } from 'redux-router';
import UIReducer from './UIReducer';
import postReducer from './postsReducer';
import accountReducer from './accountReducer';

const rootReducer = combineReducers({
  router: routerStateReducer,
  UI: UIReducer,
  account: accountReducer,
  posts: postReducer
});

export default rootReducer;
