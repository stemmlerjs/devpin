import { RECEIVED_POSTS } from '../constants/postConstants';

const defaultState = { 
  1: {
    id: 1,
    title: 'Python for Beginners',
    author: 'alexyakubovski',
    tags: ['python', 'tutorial', 'beginners'],
    link: 'http://www.google.com',
    votes: -2,
    comments: 5
  },
  2: {
    id: 2,
    title: 'DevPin',
    author: 'khalil',
    tags: ['hackathon'],
    link: 'http://www.google.com',
    votes: 213,
    comments: 119
  },
  3: {
    id: 3,
    title: 'What is Node.js?',
    author: 'talalb',
    tags: ['node', 'javascript', 'tutorial'],
    link: 'http://www.google.com',
    votes: 156,
    comments: 27
  }
};

export default function(state=defaultState, action) {
  switch(action.type) {
    case RECEIVED_POSTS:
      return { ...action.posts };
  }
  return state;
}
